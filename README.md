# DOCKER SWARM #


DOC: https://docs.docker.com/engine/swarm/

# Pré-requis :
- docker
- virttualbox

# Installation de docker-machine :
```bash
sudo curl -L https://github.com/docker/machine/releases/download/v0.4.0/docker-machine_linux-amd64 --output /usr/local/bin/docker-machine

sudo chmod +x /usr/local/bin/docker-machine

docker-machine --v
```

# Creation des workers (2):
```bash
for i in `seq 1 2`; 
do 
docker-machine create --driver virtualbox worker$i; 
done
```
# On liste les machines :
```bash
    $ docker-machine ls
```
# On créer le manager et on s'y connecte:
```bash
docker-machine create manager --driver virtualbox

docker-machine ssh manager1
```
# Init du swarm mode:
```bash
docker swarm init --advertise-addr <MANAGER-IP>
```
# Récupérer la commande de join avec le token et l'éxécuter sur chaque worker (node) via SSH:
    
    $ --token ... <MANAGER-IP>:<PORT> 

    exemple: docker swarm join --token      SWMTKN-1-2oob9x4n54ylrmy78mdp01plrh98t7ypar5r0xvpbnoh8fjxfb-aimisbk5wpeseu7d8hceflh67 192.168.99.109:2377

OU:
```bash
for i in `seq 1 2`; 
do 
docker-machine ssh worker$i docker swarm join --token <TOKEN>
<MANAGER-IP>:<PORT`;
done
```
# On retire le manager des worker:
```bash
docker node update --availability drain manager1
```
# Dans swarm mode:
```bash
docker node ls
```
# Créer 4 services Nginx:
```bash
docker service create --replicas 4 -p 8080:80 --name web nginx
```
# Se connecter en SSH a chaque worker pour modifier l'index de nginx pour identifier les workers:
```bash
docker-machine ssh worker1

echo "worker 1" > index.html

docker ps

docker cp index.html <conteneur_id>:/usr/share/nginx/html
```
  ----
```bash
docker-machine ssh worker2

echo "worker 2" > index.html

docker ps

docker cp index.html <conteneur_id>:/usr/share/nginx/html
```

# On interroge son "stack":
```bash
curl http://localhost:8080
```
# On met à l'échelle:
```bash
docker service scale web=8
```
